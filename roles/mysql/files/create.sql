CREATE TABLE `guests_count` (
`id` int(6) NOT NULL auto_increment,
`ip` varchar(15) collate utf8_unicode_ci NOT NULL default '',
`count` int(6) collate utf8_unicode_ci NOT NULL default 1,
PRIMARY KEY  (`id`),
UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;