# Nginx + PHP5.X-FPM + MySQL5.X Deployment

Expects Ununtu 14.04 hosts

These playbooks deploy a simple all-in-one configuration of the Nginx web server and the PHP-FPM process manager and MySQL database.
To use, edit the **ansible-playbook/inventory** file of hosts - include the names or URLs of the servers you want to deploy.

## Then run the playbook, like this:
```sh
$ cd ansible-lemp
```
 
```sh
$ ansible-playbook -i inventory site.yml
```

## The playbooks will configure MySQL, Nginx, and PHP-FPM. And create http://host_ip/counter.php demonstration page.
